/* 
	Developed by : Alejandro Gonzalez - V1.0
	Finish Date  : 2012-11-05
	Merges table cells only taking in account the innerHTML of the cell.
*/ 

(function( $ ){
	var TableSpanMethods = {
		horizontal : function(obj){	//The horizontal merge method
			return this.each(function() {
				$this = $(this);	//Using $this
							
				$.each($this.find("tr"), function(i, row){	//Find all the rows
					var previousTD = "X";	//Initial declaration
					$.each($(row).find("td"), function(j, column){	//Find all the columns
						if(previousTD=="X"){	//If the previous is the init value, it assigns the column to the previous read item
							previousTD = column;
						}else{
							if($(column).hasClass("ignoreTD")){	//If the column has the class ignoreTD, it skips it
								previousTD = "X";
							}else{
								if( $.trim( $(column).html().replace( "&nbsp;","" ) ) == $.trim($(previousTD).html().replace( "&nbsp;","" ) ) && $(previousTD).attr("rowspan") == $(column).attr("rowspan")){	//We remove all &nbsp; characters
									$(column).remove();	//Remove the newer column
									var colcount = 0;

									if($(previousTD).attr("colspan") == undefined){
										colcount = 1;
									}else{
										colcount = parseInt($(previousTD).attr("colspan")); 
									}
									
									$(previousTD).attr("colspan", colcount + 1)	//Increase colspan of existing column
								}else{
									previousTD = column;
								}
							}
						}
					})
				})
			})
		},

		vertical : function(obj){
			return this.each(function() {
				$this = $(this);
				var rowObject = new Object();
				var x = 0; var totalRows = 0;

				// Generate an object going cell by cell, taking in account colspan
				$.each($this.find("tr"), function(i, row){	//Find all the rows
					totalRows++;
					x = 0; rowObject[i] = new Object();
					$.each($(row).find("td"), function(j, column){	//Find all the columns
						rowObject[i][x] = column;	//Store the column on the two-dimension array

						if($(column).attr("colspan") == undefined){
							x+=1;
						}else{
							for(y = x; y < x + parseInt($(column).attr("colspan")); y++){
								rowObject[i][y] = column;
							} 
							x+=parseInt($(column).attr("colspan"))
						}
					})
				})

				$.each(rowObject, function(a,b){
					if(a < totalRows - 1){
						for(y = 0; y < x; y++){
							if( $.trim($(rowObject[a][y]).html()) == $.trim($(rowObject[parseInt(a) + 1][y]).html()) && $(rowObject[a][y]).attr("colspan") == $(rowObject[parseInt(a) + 1][y]).attr("colspan")) {
								if($(rowObject[a][y]).hasClass("ignoreTD") || $(rowObject[parseInt(a) + 1][y]).hasClass("ignoreTD")){
									var colcount = 0;
									if($(rowObject[a][y]).attr("colspan") == undefined){
										colcount = 1;
									}else{
										colcount = parseInt($(rowObject[a][y]).attr("colspan")); 
									}
									y+=colcount - 1;
								}else{
									$(rowObject[parseInt(a) + 1][y]).remove();
									rowObject[parseInt(a) + 1][y] = rowObject[parseInt(a)][y]
									var rowcount = 0;
									
									if($(rowObject[a][y]).attr("rowspan") == undefined){
										rowcount = 1;
									}else{
										rowcount = parseInt($(rowObject[a][y]).attr("rowspan")); 
									}
									
									rowcount++;
									$(rowObject[a][y]).attr("rowspan", rowcount)

									var colcount = 0;
									if($(rowObject[a][y]).attr("colspan") == undefined){
										colcount = 1;
									}else{
										colcount = parseInt($(rowObject[a][y]).attr("colspan")); 
									}
									y+=colcount - 1;
								}
							}else{
								var colcount = 0;
								if($(rowObject[a][y]).attr("colspan") == undefined){
									colcount = 1;
								}else{
									colcount = parseInt($(rowObject[a][y]).attr("colspan")); 
								}
								y+=colcount - 1;
							}
						}
					}
				});
			});
		}
	}

	$.fn.TableSpan = function(method) {
		method = method.toLowerCase();
		if ( TableSpanMethods[method] ) {
			return TableSpanMethods[method].apply( this );
		}else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error('Method ' +  method + ' does not exist on jQuery.TableSpan');
		}
	};
})( jQuery );